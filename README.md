Wadsworth Joinery is a family run business located in Unanderra, just south of Wollongong on the NSW South Coast. We have been constructing and supplying quality roof trusses, wall frames, joinery and building products since 1985 to the Illawarra, Southern Highlands and greater Sydney regions.

Address: 227 Berkeley Rd, Unanderra, NSW 2526, Australia

Phone: +61 2 4272 2611

Website: https://wadsworth.net.au
